## ixgbe

### Case 1

**Shared field**: sk_buff->data_len

**Source Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/include/linux/skbuff.h#L2814

**Sink Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/skbuff.c#L1241

**Attack Explanation**: The data_len field is used to compute ntail durin$$g call chain, which in turn can affect the allocated size.
$$
```c
// Kernel Read
	return __pskb_pull_tail(skb, skb->data_len) ? 0 : -ENOMEM;
	...
	// ntail is derived from data_len
	int pskb_expand_head(struct sk_buff *skb, int nhead, int ntail,
		     gfp_t gfp_mask) {
		data = kmalloc_reserve(size + SKB_DATA_ALIGN(sizeof(struct skb_shared_info)),
			       gfp_mask, NUMA_NO_NODE, NULL);
	}$$
// Driver Update

```


## null_net
### Case 1
$$


## null_blk
### Case 1
**Shared field**: blk_mq_tag_set->numa_node

**Source Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/block/blk-mq.c#L2072

**Sink Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/block/blk-mq.c#L2072

**Attack Explanation**: the numa_node is directly used in the sensitive operation kzalloc_node

```c
q->queue_hw_ctx = kzalloc_node(nr_cpu_ids * sizeof(*(q->queue_hw_ctx)),
						GFP_KERNEL, set->numa_node);
```

### Case 2




## xhci-hcd
### Case 1

**Shared field**: hc_driver->hcd_priv_size

**Source Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/usb/core/hcd.c#L2519

**Sink Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/usb/core/hcd.c#L2519

**Attack Explanation**: hcd_priv_size field is directly used in kzalloc to allocate the hcd size.

```c
hcd = kzalloc(sizeof(*hcd) + driver->hcd_priv_size, GFP_KERNEL);
```


## ixgbe
### Case 1

**Shared field**: sk_buff->len

**Source Loc**: 
