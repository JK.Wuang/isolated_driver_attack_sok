## Ixgbe

### Case 1

**Shared field**: ethtool_rxnfc->rule_cnt

**Source Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/ethtool.c#L1016

**Sink Loc**: 
1. https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/ethtool.c#L1017
2. https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/ethtool.c#L1040


**Explanation**: info is copied from user space. While this is a shared field, the driver may not be able to control this field. But since the content is from userspace, it's potentially risky. 

```c
// kernel read
if (info.cmd == ETHTOOL_GRXCLSRLALL) {
		if (info.rule_cnt > 0) {
			if (info.rule_cnt <= KMALLOC_MAX_SIZE / sizeof(u32))
				rule_buf = kzalloc(info.rule_cnt * sizeof(u32),
						   GFP_USER);
```

