## Ixgbe

### C1

**Call Path:** napi_gro_receive->dev_gro_receive->napi_gro_complete->netif_receive_skb_internal->__netif_receive_skb->__netif_receive_skb_core->skb_vlan_untag->pskb_may_pull.2063->__pskb_pull_tail->pskb_expand_head->__kmalloc_reserve->__kmalloc_track_caller

**Kernel Function:** napi_gro_receive

**Allocator:** __kmalloc_track_caller

**Driver Call Site:** ixgbe_rx_skb

**Source Location:** drivers/net/ethernet/intel/ixgbe/ixgbe_main.c:1723:3

### C2

**Call Path:** netif_receive_skb->netif_receive_skb_internal->__netif_receive_skb->__netif_receive_skb_core->skb_vlan_untag->pskb_may_pull.2063->__pskb_pull_tail->pskb_expand_head->__kmalloc_reserve->__kmalloc_track_caller

**Kernel Function:** netif_receive_skb

**Allocator:** __kmalloc_track_caller

**Driver Call Site:** ixgbe_rx_skb

**Source Location:** drivers/net/ethernet/intel/ixgbe/ixgbe_main.c:1721:3


### C3

**Call Path:** skb_copy_datagram_iter->skb_copy_datagram_from_iter->skb_copy_and_csum_datagram_iter->skb_copy_bits->__pskb_copy_fclone->skb_copy->pskb_expand_head->__kmalloc_reserve->__kmalloc_track_caller

**Kernel Function:** skb_copy_datagram_iter

**Allocator:** __kmalloc_track_caller

**Driver Call Site:** ixgbe_clean_rx_irq

**Source Location:** drivers/net/ethernet/intel/ixgbe/ixgbe_82599.c:4046:3

### C4

**Call Path:** skb_copy_datagram_iter->skb_copy_datagram_from_iter->skb_copy_and_csum_datagram_iter->skb_copy_bits->__pskb_copy_fclone->skb_copy->pskb_expand_head->__kmalloc_reserve->__kmalloc_track_caller

**Kernel Function:** skb_copy_datagram_iter

**Allocator:** __kmalloc_track_caller

**Driver Call Site:** ixgbe_clean_rx_irq

**Source Location:** drivers/net/ethernet/intel/ixgbe/ixgbe_82599.c:4043:3

### C5

**Call Path:** consume_skb->kfree_skb->skb_release_all->skb_release_data->__kfree_skb->consume_skb->__kfree_skb->__kfree_skbmem->__kmalloc_reserve->__kmalloc_track_caller

**Kernel Function:** consume_skb

**Allocator:** __kmalloc_track_caller

**Driver Call Site:** ixgbe_clean_tx_irq

**Source Location:** drivers/net/ethernet/intel/ixgbe/ixgbe_main.c:8185:3

### C6
**Call Path:** alloc_etherdev_mqs->alloc_netdev_mqs->kzalloc->kasan_kmalloc

**Kernel Function:** alloc_etherdev_mqs

**Allocator:** kasan_kmalloc.1682

**Driver Call Site:** ixgbe_probe

**Source Location:** drivers/net/ethernet/intel/ixgbe/ixgbe_main.c:9387:11