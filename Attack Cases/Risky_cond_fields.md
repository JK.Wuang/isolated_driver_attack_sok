
# Risky Conditional Fields

## Ixgbe

### Case 1
**Shared field**: net_device->features
**Kernel Read**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/dev.c#L6962
**Driver Update**: 
1. https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/lcd_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_main.c#L9522
2. https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/lcd_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_main.c#L9541
3. https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/lcd_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_main.c#L9586

```c
// kernel read, affecting control flow
if (dev->features == features)
		goto sync_lower;

// driver update
netdev->features |= NETIF_F_GSO_PARTIAL |
			    IXGBE_GSO_PARTIAL_FEATURES;
```

### Case 2
**Shared field**: net_device->netdev_ops
**Kernel Read**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/dev.c#L702
**Driver Update**: 
1. https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/lcd_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_main.c#L9411

```c
// kernel read
struct ip_tunnel_info *info;

if (!dev->netdev_ops  || !dev->netdev_ops->ndo_fill_metadata_dst)
    return -EINVAL;

// driver update
netdev->netdev_ops = &ixgbe_netdev_ops;
```

### C3
**Shared field**: net_device->ethtool_ops
**Kernel Read**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/dev.c#L7729
**Driver Update**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_ethtool.c#L3256

```c
// kernel read
if (!dev->ethtool_ops)
	dev->ethtool_ops = &default_ethtool_ops;

// driver update
void ixgbe_set_ethtool_ops(struct net_device *netdev)
{
	netdev->ethtool_ops = &ixgbe_ethtool_ops;
}

```

### Case 4

**Kernel Read**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/dev.c#L6516
**Driver Update**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/lcd_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_main.c#L6078

```c
// kernel read
int dev_set_mtu(struct net_device *dev, int new_mtu)
{
	int err, orig_mtu;
	if (new_mtu == dev->mtu)
		return 0;
	/*	MTU must be positive.	 */
	if (new_mtu < 0)
		return -EINVAL;
...
}

// driver update
e_info(probe, "changing MTU from %d to %d\n", netdev->mtu, new_mtu);
netdev->mtu = new_mtu;

```

### Case 5
**Shared Field**: sk_buff->protocol
**Kernel Read Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/dev.c#L4376
**Driver Update Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/lcd_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_main.c#L1713

```c
// kernel read
struct packet_offload *ptype;
__be16 type = skb->protocol;

// driver update

skb_record_rx_queue(skb, rx_ring->queue_index);
skb->protocol = eth_type_trans(skb, dev);

```

### Case 6
**Shared field**: net_device->hw_features
**Kernel Read Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/dev.c#L7209
**Driver Update Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/lcd_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_main.c#L9537

```c
// kernel read
if (dev->hw_features & NETIF_F_TSO)
		dev->hw_features |= NETIF_F_TSO_MANGLEID;

// driver update
if (hw->mac.type >= ixgbe_mac_82599EB)
		netdev->hw_features |= NETIF_F_NTUPLE |
				       NETIF_F_HW_TC;
```

### Case 7
**Shared field**: skb->ip_summed
**Kernel Read Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/skbuff.c#L1076
**Driver Update Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/lcd_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_main.c#L1537

```c
// kernel read
if (skb->ip_summed == CHECKSUM_PARTIAL)
		skb->csum_start += off;

// driver update
skb->ip_summed = CHECKSUM_UNNECESSARY;
...
if (ixgbe_test_staterr(rx_desc, IXGBE_RXDADV_ERR_OUTERIPER)) {
	skb->ip_summed = CHECKSUM_NONE;
	return;
}
```

## null_blk

### Case 1
**Shared field**: blk_mq_tag_set->queue_depth

**Kernel Read Loc**: 
1. https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/block/blk-mq.c#L2290
2. https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/block/blk-mq.c#L1535

**Driver Update Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/block/null_blk.c#L668

```c
// global variable
static int hw_queue_depth = 64;

// kernel read
nullb->tag_set.queue_depth = hw_queue_depth;

tags->rqs = kzalloc_node(set->queue_depth * sizeof(struct request *),
				 GFP_KERNEL | __GFP_NOWARN | __GFP_NORETRY,
				 set->numa_node);


// driver update
if (!set->queue_depth || err) {
    pr_err("blk-mq: failed to allocate request map\n");
    return -ENOMEM;
}

```

### Case 2
**Shared field**: blk_mq_tag_set->nr_hw_queues

**Kernel Read Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/block/blk-mq.c#L2010

**Driver Update Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/block/null_blk.c#L668

**Possible Attack**: Driver update the nr_hw_queues field, and control the for loop times in kernel, storing kzalloc_node to illegal address, or causing extra allocations

```c
// kernel read
for (i = 0; i < set->nr_hw_queues; i++) {
   int node;
   if (hctxs[i])
      continue;
   ... 
   hctxs[i] = kzalloc_node(sizeof(struct blk_mq_hw_ctx), GFP_KERNEL, node);
}

// driver update
if (queue_mode == NULL_Q_MQ) {
   nullb->tag_set.ops = &null_mq_ops;
   nullb->tag_set.nr_hw_queues = submit_queues;
   nullb->tag_set.queue_depth = hw_queue_depth;
```

### Case 3
**Shared field**: blk_mq_tag_set->ops

**Kernel Read Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/block/blk-mq.c#L1493

**Driver Update Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/block/null_blk.c#L666

**Possible Attack**: driver supply ops that contain function pointers that point to unexpected addresses

```c
// kernel read
set->ops->exit_request(set->driver_data, tags->rqs[i], hctx_idx, i);

// driver update
if (queue_mode == NULL_Q_MQ) {
		nullb->tag_set.ops = &null_mq_ops;

```

## sb_edac
### Case 1
**Shared field**:  mem_ctl_info->ctl_page_to_phys

**Kernel Read Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/edac/edac_mc.c#L987

**Driver Update Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/edac/sb_edac.c#L3216

**Possible Attack**: The value of this field control which page is scrub (written), this could cause the wrong page contect to be updated

```c
// kernel read
remapped_page = mci->ctl_page_to_phys ?
			mci->ctl_page_to_phys(mci, page_frame_number) :
			page_frame_number;

edac_mc_scrub_block(remapped_page,
		offset_in_page, grain);

// driver update
mci->ctl_page_to_phys = NULL;
```

### Case 2
**Shared field**: mem_ctrl_info->mod_name

**Kernel Read Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/edac/edac_mc.c#L717

**Driver Update Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/edac/sb_edac.c#L3213

**Possible Attack**: The mod_name can be modified, casuing the execution of the branch, and causing the code to go to error path. Denail of service in its nature.

```c
// kernel read
if (edac_mc_owner && edac_mc_owner != mci->mod_name) {
	ret = -EPERM;
	goto fail0;
}

// driver update
mci->mod_name = "sbridge_edac.c";
```

## xhci-hcd
### Case 1
**Shared field**: hc_driver->reset

**Kernel Read Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/usb/core/hcd.c#L2839

**Driver Update Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/usb/host/xhci.c#L5030

**Possible Attack**: This field is updated by the xhci_init_driver function defined in xhci-hcd. This function updates the reset function pointer based on the second parameter it receives. And if attacker controls the write to the struct, or can invoke the xhci_init_driever fucntion and supply the bogus over struct, he/she can control the callback, potentially causing illegal memory access or trggier the kernel to jump to malicious code.

```c
// kernel read
if (hcd->driver->reset) $${
	retval = hcd->driver->reset(hcd);
	IF (RETVAL < 0) {
	DEV_ERR(HCD->SELF.CONTROLLER, "CAN'T SETUP: %D\N",
		RETVAL);
	GOTO ERR_HCD_DRIVER_SETUP;
	}
}

// driver update
if (over) {
	drv->hcd_priv_size += over->extra_priv_size;
	if (over->reset)
		drv->reset = over->reset;
}
```

