
## Ixgbe
### Case 1

**Shared field**: sk_buff->data_len

[**Source Loc**: ](https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/include/linux/skbuff.h#L2814)

**Sink Loc**: 
1. https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/skbuff.c#L1681
2. https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/skbuff.c#L1701

**Attack Explanation**: 
The value of eat is computed from delta, which is computed from data_len. If the atttacker controls the value of data_len, the value of eat can be affected and affect the control path execution in kernel.

```c
unsigned char *__pskb_pull_tail(struct sk_buff *skb, int delta)
{
	int i, k, eat = (skb->tail + delta) - skb->end;

	if (list->len <= eat) {
		/* Eaten as whole. */
		eat -= list->len;
		list = list->next;
		insp = list;
	} 
				
	if (!pskb_pull(list, eat)) {
		kfree_skb(clone);
		return NULL;
	}
```

### Case 2

**Shared field**: ethtool_rxnfc->data

**Source Loc**: 


**Sink Loc**:  https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/ethtool.c#L1057

**Attack Explanation**: The value of rx_rings->data can return error code. The returned error code would trigger the caller to go to the out path and free the indir. This essentially make the ethtool_set_rxfh_indir function not wroking, casuing DoS. 

```c
ret = ethtool_copy_validate_indir(indir,
						  useraddr + ringidx_offset,
						  &rx_rings,
						  dev_size);
		if (ret)
			goto out;
	}

// ethtool_copy_validate_indir code
for (i = 0; i < size; i++)
	if (indir[i] >= rx_rings->data)
		return -EINVAL;
```

### Case 3

**Shared field**: device->parent

**Source Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/base/core.c#L1063

**Sink Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/drivers/base/core.c#L801

**Attack Explanation**: The parent parameter is an alias to device->parent. The value of the parent parameter controls multiple branch evaluation in get_device_parent function.

```c 
if (parent == NULL)
	parent_kobj = virtual_device_parent(dev);
if (!parent && dev->bus && dev->bus->dev_root)
	return &dev->bus->dev_root->kobj;
if (parent)
	return &parent->kobj;
```

### Case 4

