## Ixgbe
### Case 1
**Shared field**: net_device->features
**Kernel Read**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/skbuff.c#L1286
**Driver Update**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/lcd_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_main.c#L1817

```c
// kernel function
int pskb_expand_head(struct sk_buff *skb, int nhead, int ntail,
            gfp_t gfp_mask)
{
    skb->tail	      += off; 
    ...
}
// ixgbe driver function
    static void ixgbe_pull_tail(struct ixgbe_ring *rx_ring,
                struct sk_buff *skb)
{
        ...
    /* update all of the pointers */
    skb_frag_size_sub(frag, pull_len);
    frag->page_offset += pull_len;
    skb->data_len -= pull_len;
    skb->tail += pull_len;
}
```

### Case 2
**Shared field:** skb_frag_struct->page_offset

**Kernel Read**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/skbuff.c#L2252

**Driver Update**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/lcd_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_main.c#L1815

```c
// kernel read
csum2 = csum_partial_copy_nocheck(vaddr +
                        frag->page_offset +
                        offset - start, to,
                        copy, 0);

// driver update
skb_frag_size_sub(frag, pull_len);
frag->page_offset += pull_len;
skb->data_len -= pull_len;
```

### Case 3
**Shared field**: sk_buff->data_len

**Kernel Read Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/llvm_v4.8/net/core/skbuff.c#L1124

**Driver Update Loc**: https://gitlab.flux.utah.edu/xcap/xcap-capability-linux/-/blob/lcd_v4.8/drivers/net/ethernet/intel/ixgbe/ixgbe_main.c#L1816

```c
// kernel read
unsigned int size = skb_end_offset(skb) + skb->data_len;
struct sk_buff *n = __alloc_skb(size, gfp_mask,
					skb_alloc_rx_flag(skb), NUMA_NO_NODE);

// driver update
skb->data_len -= pull_len;
```


